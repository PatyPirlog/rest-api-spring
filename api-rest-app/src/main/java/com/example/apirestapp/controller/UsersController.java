package com.example.apirestapp.controller;

import com.example.apirestapp.model.City;
import com.example.apirestapp.model.Country;
import com.example.apirestapp.model.Region;
import com.example.apirestapp.model.Sport;
import com.example.apirestapp.services.AppService;
import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

@RestController
@RequestMapping("/countries")
public class UsersController {

    @Autowired
    private AppService myService;

    // method to add a country in database
    @PostMapping(value = "/load")
    public ResponseEntity<Response> load(@RequestBody final Country country) {
        Country current = myService.addCountry(country);
        return new ResponseEntity(current, HttpStatus.CREATED);
    }

    // method to add a city in database
    @PostMapping(value = "/load/cities")
    public ResponseEntity<Response> loadCities(@RequestBody final City city) {
        City current = myService.addCity(city);
        myService.getRegionById(city.getId_region()).ifPresent(cnt -> cnt.addCity(city));

        return new ResponseEntity(current, HttpStatus.CREATED);
    }

    // method to add a region in database
    @PostMapping(value = "/load/regions")
    public ResponseEntity<Response> loadRegions(@RequestBody final Region region) {
        Region current = myService.addRegion(region);
        myService.getCountryById(region.getId_country()).ifPresent(cnt -> cnt.addRegion(region));

        return new ResponseEntity(current, HttpStatus.CREATED);
    }

    // method to add a sport in database
    @PostMapping(value = "/load/sports")
    public ResponseEntity<Response> loadSports(@RequestBody final Sport sport) {
        sport.setStartDate();
        sport.setEndDate();
        Sport current = myService.addSport(sport);

        for (Long id_city : sport.getId_cities()) {
            myService.getCityById(id_city).ifPresent(cnt -> cnt.addSport(sport));
        }

        return new ResponseEntity(current, HttpStatus.CREATED);
    }

    // method to find a country by name and show it
    @GetMapping(value = "/{name}")
    public ResponseEntity<Response> findByName(@PathVariable final String name) {
        Country country = myService.getCountryByName(name);
        return new ResponseEntity(country, HttpStatus.OK);
    }

    // method to get all countries from database
    @GetMapping(value = "/all")
    public ResponseEntity<Response> findAll() {
        List<Country> myList = myService.getCountries();
        return new ResponseEntity(myList, HttpStatus.OK);
    }


    // method to find all regions in database
    @GetMapping(value = "/all/regions")
    public ResponseEntity<Response> findAllRegions() {
        List<Region> myList = myService.getRegions();
        return new ResponseEntity(myList, HttpStatus.OK);
    }

    // method to get all cities in database
    @GetMapping(value = "/all/cities")
    public ResponseEntity<Response> findAllCities() {
        List<City> myList = myService.getCities();
        return new ResponseEntity(myList, HttpStatus.OK);
    }

    // get all available sports in database
    @GetMapping(value = "/all/sports")
    public ResponseEntity<Response> findAllSports() {
        List<Sport> myList = myService.getSports();
        return new ResponseEntity(myList, HttpStatus.OK);
    }

    // method to show all sports available in a specified city
    @GetMapping(value = "all/sports/{name}")
    public ResponseEntity<Response> getSportsForCity(@PathVariable final String name) {
        List<Sport> myList = myService.getCityByName(name).getSports();
        return new ResponseEntity(myList, HttpStatus.OK);
    }

    // method to get a list of all the cities where a sport is available
    @GetMapping(value = "all/sports/cities/{name}")
    public ResponseEntity<Response> getCitiesForSports(@PathVariable final String name) {
        List<City> mylist = new ArrayList<City>();

        for (City city : myService.getCities()) {
            for (Sport s : city.getSports()) {
                if ((s.getName().equals(name))) {
                    mylist.add(city);
                }
            }
        }

        return new ResponseEntity(mylist, HttpStatus.OK);
    }

    // method to get all regions in which a sport is available
    @GetMapping(value = "all/sports/regions/{name}")
    public ResponseEntity<Response> getRegionsForSports(@PathVariable final String name) {
        Set<String> myList = new HashSet<String>();

        for (Region region : myService.getRegions()) {
            for (City city : region.getCities()) {
                for (Sport s : city.getSports()) {
                    if (s.getName().equals(name))
                        myList.add(region.getName());
                }
            }
        }

        return new ResponseEntity(myList, HttpStatus.OK);
    }

    // method to get all regions where a sport is available in the secified period
    @GetMapping(value = "all/sports/{sport}/{startDate}/{endDate}")
    public ResponseEntity<Response> getSportsForPeriod(@PathVariable final String sport,
                                                       @PathVariable final String startDate,
                                                       @PathVariable final String endDate) {
        LocalDate startD, endD;
        Set<City> myList = new HashSet<City>();

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
            startD = LocalDate.parse(startDate, formatter);
            endD = LocalDate.parse(endDate, formatter);
        } catch (DateTimeParseException e) {
            return null;
        }

        for (Region region : myService.getRegions()) {
            for (City city : region.getCities()) {
                for (Sport s : city.getSports()) {
                    if (s.getName().equals(sport) && !s.getStartDate().isAfter(startD)
                            && !s.getEndDate().isBefore(endD))
                        myList.add(city);
                }
            }
        }

        return new ResponseEntity(myList, HttpStatus.OK);
    }

    // method to update a region in database (id needed)
    @PatchMapping(value = "/load/regions/{id}")
    public ResponseEntity<Response> updateRegion(@RequestBody final Region partialRegion,
                                                 @PathVariable final Long id) {
        return new ResponseEntity(myService.updateRegion(id, partialRegion), HttpStatus.OK);
    }

    // method to update a city in database (id needed)
    @PatchMapping(value = "/load/cities/{id}")
    public ResponseEntity<Response> updateCity(@RequestBody final City partialCity,
                                               @PathVariable final Long id) {
        return new ResponseEntity(myService.updateCity(id, partialCity), HttpStatus.OK);
    }

    // method to update a sport in database (id needed)
    @PatchMapping(value = "/load/sports/{id}")
    public ResponseEntity<Response> updateSport(@RequestBody final Sport partialSport,
                                                @PathVariable final Long id) {
        return new ResponseEntity(myService.updateSport(id, partialSport), HttpStatus.OK);
    }

    // find and delete a sport by id (id needed)
    @DeleteMapping(value = "/delete/sport/{id}")
    public ResponseEntity<Response> deleteSport(@PathVariable final Long id) {
        return new ResponseEntity(myService.deleteSport(id), HttpStatus.OK);
    }

    // find and delete city by id (id needed)
    @DeleteMapping(value = "/delete/city/{id}")
    public ResponseEntity<Response> deleteCity(@PathVariable final Long id) {
        return new ResponseEntity(myService.deleteCity(id), HttpStatus.OK);
    }

}

package com.example.apirestapp.repository;

import com.example.apirestapp.model.Country;
import com.example.apirestapp.model.Region;
import com.example.apirestapp.model.Sport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface SportRepository extends JpaRepository<Sport, Long> {

    Sport findByName(String name);

    Long deleteByName(String name);
}
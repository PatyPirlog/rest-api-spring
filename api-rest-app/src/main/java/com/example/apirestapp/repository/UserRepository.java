package com.example.apirestapp.repository;

import com.example.apirestapp.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface UserRepository extends JpaRepository<Country, Long> {

    Country findByName(String name);

    Long deleteByName(String name);
}

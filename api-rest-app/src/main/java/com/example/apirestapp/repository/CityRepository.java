package com.example.apirestapp.repository;

import com.example.apirestapp.model.City;
import com.example.apirestapp.model.Country;
import com.example.apirestapp.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface CityRepository extends JpaRepository<City, Long> {

    City findByName(String name);

    Long deleteByName(String name);
}
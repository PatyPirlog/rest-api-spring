package com.example.apirestapp.services;

import com.example.apirestapp.model.City;
import com.example.apirestapp.model.Country;
import com.example.apirestapp.model.Region;
import com.example.apirestapp.model.Sport;
import com.example.apirestapp.repository.CityRepository;
import com.example.apirestapp.repository.RegionRepository;
import com.example.apirestapp.repository.SportRepository;
import com.example.apirestapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AppService {

    @Autowired
    private UserRepository userJPArepo;

    @Autowired
    private RegionRepository regionJPArepo;

    @Autowired
    private CityRepository cityJPArepo;

    @Autowired
    private SportRepository sportJPArepo;

    public List<Country> getCountries() {
        return userJPArepo.findAll();
    }

    public Country getCountryByName(final String name) {
        return userJPArepo.findByName(name);
    }

    public Optional<Country> getCountryById(final Long id) {
        return userJPArepo.findById(id);
    }

    public Optional<Region> getRegionById(final Long id) {
        return regionJPArepo.findById(id);
    }

    public Optional<City> getCityById(final Long id) {
        return cityJPArepo.findById(id);
    }

    public Country addCountry(final Country country) {
        userJPArepo.save(country);
        return userJPArepo.findByName(country.getName());
    }

    public void deleteCountry(final String name) {
        userJPArepo.deleteByName(name);
    }

    public List<Region> getRegions() {
        return regionJPArepo.findAll();
    }

    public Region getRegionByName(final String name) {
        return regionJPArepo.findByName(name);
    }

    public Region addRegion(final Region region) {
        regionJPArepo.save(region);
        return regionJPArepo.findByName(region.getName());
    }

    public Region updateRegion(final Long id, final Region region) {
        Region myRegion = regionJPArepo.findById(id).get();

        myRegion.setName(region.getName());
        regionJPArepo.save(myRegion);

        return myRegion;
    }

    public City updateCity(final Long id, final City city) {
        City myCity = cityJPArepo.findById(id).get();

        myCity.setName(city.getName());
        cityJPArepo.save(myCity);

        return myCity;
    }

    public Sport updateSport(final Long id, final Sport sport) {
        Sport mySport = sportJPArepo.findById(id).get();

        mySport.setName(sport.getName());
        mySport.setStartDate(sport);
        mySport.setEndDate(sport);
        mySport.setCost(sport.getCost());
        sportJPArepo.save(mySport);

        return mySport;
    }

    public void deleteRegion(final String name) {
        regionJPArepo.deleteByName(name);
    }

    public List<City> getCities() {
        return cityJPArepo.findAll();
    }

    public City getCityByName(final String name) {
        return cityJPArepo.findByName(name);
    }

    public City addCity(final City city) {
        cityJPArepo.save(city);
        return cityJPArepo.findByName(city.getName());
    }

    public void deleteCity(final String name) {
        cityJPArepo.deleteByName(name);
    }

    public List<Sport> getSports() {
        return sportJPArepo.findAll();
    }

    public Sport getSportByName(final String name) {
        return sportJPArepo.findByName(name);
    }

    public Sport addSport(final Sport sport) {
        sportJPArepo.save(sport);
        return sportJPArepo.findByName(sport.getName());
    }

    public Long deleteSport(final Long id) {
        sportJPArepo.deleteById(id);
        return id;
    }

    public Long deleteCity(final Long id) {
        cityJPArepo.deleteById(id);
        return id;
    }
}

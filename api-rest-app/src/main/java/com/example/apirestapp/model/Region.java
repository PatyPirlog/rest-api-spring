package com.example.apirestapp.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Region {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long id_country;
    @OneToMany(mappedBy = "id_region")
    private List<City> cities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCities() {
        return this.cities;
    }

    public City getCity(String name) {
        return this.cities.stream()
                .filter(r -> r.getName().
                        equals(name)).findAny().orElse(null);
    }

    public void addCity(City city) {
        if (this.cities == null)
            this.cities = new ArrayList<City>();
        this.cities.add(city);
    }

    public void addCities(List<City> cities) {
        if (this.cities == null) {
            this.cities = new ArrayList<City>();
        }
        this.cities.addAll(cities);
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public Long getId_country() {
        return id_country;
    }

    public void setId_country(Long id_country) {
        this.id_country = id_country;
    }

}

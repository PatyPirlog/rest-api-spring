package com.example.apirestapp.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @OneToMany(targetEntity = Region.class, mappedBy = "id_country", fetch = FetchType.EAGER)
    private List<Region> regions = new ArrayList<Region>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addRegions(List<Region> regions) {
        if (this.regions == null) {
            this.regions = new ArrayList<Region>();
        }
        this.regions.addAll(regions);
    }

    public void addRegion(Region region) {
        if (this.regions == null) {
            this.regions = new ArrayList<Region>();
        }
        this.regions.add(region);
    }

    public List<Region> getRegions() {
        return regions;
    }

    public Region getRegion(String name) {
        return this.regions.stream()
                .filter(r -> r.getName().
                        equals(name)).findAny().orElse(null);
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

}

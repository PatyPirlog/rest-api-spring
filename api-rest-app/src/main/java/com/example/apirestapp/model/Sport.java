package com.example.apirestapp.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Sport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String startDateString;
    private String endDateString;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer cost;
    @ElementCollection
    private List<Long> id_cities = new ArrayList<Long>();

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getId_cities() {
        return id_cities;
    }

    public void setId_cities(List<Long> id_cities) {
        this.id_cities = id_cities;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate() {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
            this.startDate = LocalDate.parse(this.startDateString, formatter);
        } catch (DateTimeParseException e) {
            return;
        }
    }

    public void setStartDate(Sport sport) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
            this.startDateString = sport.getStartDateString();
            this.startDate = LocalDate.parse(sport.getStartDateString(), formatter);
        } catch (DateTimeParseException e) {
            return;
        }
    }

    public String getStartDateString() {
        return startDateString;
    }

    public String getEndDateString() {
        return endDateString;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate() {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
            this.endDate = LocalDate.parse(this.endDateString, formatter);
        } catch (DateTimeParseException e) {
            return;
        }
    }

    public void setEndDate(Sport sport) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
            this.endDateString = sport.getEndDateString();
            this.endDate = LocalDate.parse(sport.getEndDateString(), formatter);
        } catch (DateTimeParseException e) {
            return;
        }
    }

    public String toString() {
        return this.id + " " + this.name + " orase: " + this.id_cities;
    }

}

package com.example.apirestapp.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long id_region;

    @ManyToMany(mappedBy = "id_cities")
    private List<Sport> sports = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sport getSport(String name) {
        return this.sports.stream()
                .filter(r -> r.getName().
                        equals(name)).findAny().orElse(null);
    }

    public List<Sport> getSports() {
        return this.sports;
    }

    public void addSport(Sport sport) {
        if (this.sports == null)
            this.sports = new ArrayList<Sport>();
        this.sports.add(sport);
        System.out.println("adaugat");
        System.out.println(this.sports.toString());
    }

    public void addSports(List<Sport> sports) {
        if (this.sports == null) {
            this.sports = new ArrayList<Sport>();
        }
        this.sports.addAll(sports);
    }

    public void setSports(List<Sport> sports) {
        this.sports = sports;
    }

    public Long getId_region() {
        return id_region;
    }

    public void setId_region(Long id_region) {
        this.id_region = id_region;
    }

    public String toString() {
        return this.id + " " + this.name + " " + this.sports;
    }

}

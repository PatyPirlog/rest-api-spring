Pirlog Patricia - REST Api Application

IDE & Tools: IntelliJ, Postman
This project was created using Java (java-version 11) and Spring Framework.

I have also attached a file with input JSON entities in order to test the app
using Postman.

The database contains countries, regions, cities and sports and their relation is
nested(A country has many regions, a region has many cities and a city has many sports).

The id for each entity is generated automatically.

More details about what every method/request does are explained in the source code
comments.